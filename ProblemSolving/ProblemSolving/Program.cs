﻿using System.Text;

int n = Convert.ToInt32(Console.ReadLine());
string[] words = new string[n];
string[] finalWords = new string[n];

for (int i = 0; i < n; i++)
{
    words[i] = Console.ReadLine();

    // process for abbreviation 
    finalWords[i] = ProcessWord(words[i]);
}

for (int i = 0; i < n; i++)
{
    Console.WriteLine(finalWords[i]);
}


static string ProcessWord(string word)
{
    if (!string.IsNullOrEmpty(word))
    {
        if (word.Length > 10)
        {
            StringBuilder finalWord = new StringBuilder();
            char firstChar = word.ElementAt(0);
            char lastChar = word.ElementAt(word.Length - 1);

            finalWord.Append(word.ElementAt(0));
            finalWord.Append(word.Length - 2);
            finalWord.Append(word.ElementAt(word.Length - 1));

            return finalWord.ToString();
        }

        return word;
    }

    return null;
}
